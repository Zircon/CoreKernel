;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011 Maxime Jeanson for the Zircon Project.                    ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;----------------------------------kterm.asm---------------------------------;;
;;                                                                            ;;
;; This file provide functions and definitions for the core terminal service  ;;
;; of the system. It also provide the basic terminal system for the debug     ;;
;; mode.                                                                      ;;
;;                                                                            ;;
;;----------------------------------functions---------------------------------;;
;;                                                                            ;;
;; void kprintf(ktermMode, str*) var1, var*                                   ;;
;;                                                                            ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


%ifndef __F_CORE_KTERM_ASM__
%define __F_CORE_KTERM_ASM__

	%ifndef __F_CORE_ASM__
	%define __F_CORE_ASM__
		[ORG 0x100000]
		[BITS 32]
		align 1
	%endif

    %include "core/kterm.inc"
    
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;--------------------------------kprintf---------------------------------;;
    ;;                                                                        ;;
    ;; This function allow printing texts on the screen without the C style.  ;;
    ;; The above example will show you how to use it.                         ;;
    ;; ;; Some code.                                                          ;;
    ;; u16 var16, 0x2216                                                      ;;
    ;; str msg, "This text will be put on the screen ", hex16, ".", cr, 0x00  ;;
    ;; kprintf(kterm_normal, msg) var16                                       ;;
    ;; ;; Next some code.                                                     ;;
    ;;                                                                        ;;
    ;; The above code will put on the screen the text: "This text will be put ;;
    ;; on the screen 0x2216.\n\0". NOTE: the \n and the \0 are here in the C  ;;
    ;; style for simplicity only.                                             ;;
    ;;                                                                        ;;
    ;;---------------------------------stack----------------------------------;;
    ;;                                                                        ;;
    ;; ╔═══════════════════╗    This is for explanation only.                 ;;
    ;; ║        ...        ║     - Something before.                          ;;
    ;; ║     ktermMode     ║     - The terminal mode of 16 bits long.         ;;
    ;; ║        str*       ║     - The string pointer to print.               ;;
    ;; ║       n_var       ║     - The number of vars of 16 bits.             ;;
    ;; ║       var...      ║     - The vars pointer (That MUST BE IN ORDER).  ;;
    ;; ║        ...        ║     - The std call parameters.                   ;;
    ;; ╚═══════════════════╝                                                  ;;
    ;;                                                                        ;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    __kprintf:
        push ebp
        mov ebp, esp
        mov esi, [ebp + 0x08]

        .checking:
            ; Initialization
            mov al, byte [ esi ]
            inc esi

            ; Checking
            cmp al, null
            je .ret

;           cmp al, hex
;           je .hex

            cmp al, tab
            je .tab

            cmp al, lf
            je .lf

            cmp al, vt
            je .vt

            cmp al, cr
            je .cr

        ;=======================================================================
        ; This part put any printable char on the screen at the current cursor
        ; position.
        ;=======================================================================
        .char:
            xor ebx, ebx                        ; We clear the ebx register and
            mov bx,  word  [kterm.cursor]       ; load the current cursor
            shl ebx, byte  0x01                 ; position. We shift left it to
            add ebx, dword [kterm.monitor.base] ; multiply per two and add it
                                                ; the base address of the video
                                                ; memory

            mov ah,  [ebp + 0x0c]               ; Load the attribute of the
                                                ; char to ah. Please see the
                                                ; explaination in the vga-doc.

            mov word [ebx], ax                  ; Put the char on the screen
            inc word [kterm.cursor]             ; and than inc the cursor
                                                ; position.

            jmp .checking                       ; Go back check the next char.

        ;-----------------------------------------------------------------------

        .tab:
            add [kterm.cursor], word 0x0004

            jmp .checking

        ;-----------------------------------------------------------------------

        .lf:
            mov ax, word [kterm.cursor]

            mov bx, word 0x00a0

            div bx
            mul bx

            mov word [kterm.cursor], ax

            jmp .checking

        ;-----------------------------------------------------------------------

        .vt:
            add [kterm.cursor], word 0x00A0

            jmp .checking

        ;=======================================================================
        ; Do an carriage return.
        ;=======================================================================
        .cr:
            mov ax, word [kterm.cursor]     ; Load the cursor position, set the
            mov bx, word 0x0050             ; x size of the screen to bx and 
            div bx                          ; than divide ax by it to obtain
                                            ; the current line number.
                                            
            inc ax                          ; inc ax to the next line and than
            mul bx                          ; multiply by bx to get the new
                                            ; cursor position.

            mov word [kterm.cursor], ax     ; Save it.

            jmp .checking

        ;-----------------------------------------------------------------------

        .ret:
            pop ebp
            jmp kterm.setCur

    ;---------------------------------------------------------------------------

    kterm.setCur:
        mov bx, word [kterm.cursor]
        
        mov dx, 0x03D4      ; Affecte la valeur 0x03D4 au registre dx.
        mov al, 0x0E        ; Affecte la valeur 0x0E au registre al.
        out dx, al          ; Envoie de al au port de sortie dx.
            
        mov dx, 0x03D5      ; Affecte la valeur 0x03D5 au registre dx.
        mov al, bh          ; Affecte le registre BH au registre al.
        out dx, al          ; Envoie de al au port de sortie dx.

        mov dx, 0x03D4      ; Affecte la valeur 0x03D4 au registre dx.
        mov al, 0x0F        ; Affecte la valeur 0x0F au registre al.
        out dx, al          ; Envoie de al au port de sortie dx.

        mov dx, 0x03D5      ; Affecte la valeur 0x03D5 au registre dx.
        mov al, bl          ; Affecte le registre BL au registre al.
        out dx, al          ; Envoie de al au port de sortie dx.

        ret


%endif ; __F_CORE_KTERM_ASM__
