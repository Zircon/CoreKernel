%ifndef __F_CORE_HAL_IRS_ASM__
%define __F_CORE_HAL_IRS_ASM__

	%include 'core/kterm.inc'

	;---------------------------------------------------------------------------
	; Interrupt 0: Devide Error Exception (DE)
	;---------------------------------------------------------------------------
	__irs_de:
		str .error, 'Division by zero at: ', 0x01, ':', 0x01, '.', CR, 0x00
		
		pushad
		mov ebp, esp
		mov eax, [ebp + 0x20]	; EIP.
		mov bx,  [ebp + 0x24]	; CS.
		
		kprintf(kterm_warning, .error, bx, eax)
		
		mov al, 0x20
		out 0x20, al
		
		popad
		
		iretd

	;---------------------------------------------------------------------------
	; Interrupt 1: Devide Error Exception (DE)
	;---------------------------------------------------------------------------
;	__irs_de:
;		str .error, 'Division by zero at: ', 0x01, ':', 0x01, '.', CR, 0x00
;		
;		pushad
;		mov ebp, esp
;		mov eax, [ebp]			; EIP.
;		mov bx,  [ebp + 0x04]	; CS.
;		
;		kprintf(kterm_warning, .error, bx, eax)
;		
;		mov al, 0x20
;		out 0x20, al
;		
;		popad
;		
;		iretd


%endif ; __F_CORE_HAL_IRS_ASM__
