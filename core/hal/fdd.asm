%include "core/kterm.asm"

str fdType.np, "not present", 0x00
str fdType.pt1, "present; 360kb 5.25in", 0x00
str fdType.pt2, "present; 1.2mb 5.25in", 0x00
str fdType.pt3, "present; 720kb 3.5in", 0x00
str fdType.pt4, "present; 1.44mb 3.5in", 0x00
str fdType.pt5, "present; 2.88mb 3.5in", 0x00

str fdd.msga, "The drive A:\ is ", 0x00
str fdd.msgb, "The drive B:\ is ", 0x00

uint32 fdType, {fdType.np, fdType.pt1, fdType.pt2, fdType.pt3, fdType.pt4, fdType.pt5}


__flpyDriveDetect:
    mov dx, 0x70
    mov al, 0x10
    out dx, al

    mov dx, 0x71
    in al, dx

    push ax
    and eax, 0xf0
    shr al, 0x02
    mov edx, [eax + fdType]

    kprintf(kterm.normal, fdd.msga)
    kprintf(kterm.normal, edx)
    

    pop ax
    and eax, 0x0f
    shl al, 0x02
    mov edx, [eax + fdType]

    kprintf(kterm.normal, fdd.msgb)
    kprintf(kterm.normal, edx)
    
    ret
