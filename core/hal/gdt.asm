;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright � 2011 Maxime Jeanson for the Zircon Project.                    ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;----------------------------------gdt.asm-----------------------------------;;
;;                                                                            ;;
;; This file contain the hardware abstraction layer for the default gdt       ;;
;; initialization and management system. These functions word for Intel� and  ;;
;; AMD�.                                                                      ;;
;;                                                                            ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef __F_CORE_HAL_GDT_ASM__
%define __F_CORE_HAL_GDT_ASM__
	
	CPU x64
	
	%include "include/core/hal/gdt.inc"

	__gdtInit:
		
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; 12 16 #desc
	;; 0e 32 BaseAddr
	;; 0a 32 Limit
	;; 08 16 type
	__gdtNewDesc:
		push ebp
		mov ebp, esp
		
		xor edx, edx
		mov word dx, word [ebp + 0x12]
		
		mov ax, word [ebp + 0x0e]
		mov [(edx*8)+0x02], word ax

		mov ax, word [ebp + 0x10]
		mov [(edx*8)+0x04], byte al
		mov [(edx*8)+0x07], byte ah
		
		mov ax, word [ebp + 0x0a]
		mov [(edx*8)], word ax
		
		mov al, byte [ebp + 0x0c]
		mov bx, word [ebp + 0x08]
		shl bh, 0x04
		
		and bh, 0xf0
		and al, byte 0x0f
		
		or al, bh
		mov byte [(edx*8)+0x06], al
		mov byte [(edx*8)+0x05], bl
		
		mov eax, 0x12345678
		
		pop ebp
		ret
		
		
		

%endif ; __F_CORE_HAL_GDT_ASM__
	
