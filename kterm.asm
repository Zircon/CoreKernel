;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright � 2011 Maxime Jeanson for the Zircon Project.                    ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;----------------------------------kterm.asm---------------------------------;;
;;                                                                            ;;
;; This file provide functions and definitions for the core terminal service  ;;
;; of the system. It also provide the basic terminal system for the debug     ;;
;; mode.                                                                      ;;
;;                                                                            ;;
;;----------------------------------functions---------------------------------;;
;;                                                                            ;;
;; void kprintf(ktermMode, str*) var1, var*                                   ;;
;;                                                                            ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


%ifndef __F_CORE_KTERM_ASM__
%define __F_CORE_KTERM_ASM__

;    %include "core/kterm.inc"
    
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;--------------------------------kprintf---------------------------------;;
    ;;                                                                        ;;
    ;; This function allow printing texts on the screen with the C style. The ;;
    ;; above example will show you how to use it.                             ;;
    ;; ;; Some code.                                                          ;;
    ;; u16 var16, 0x2216                                                      ;;
    ;; str msg, "This text will be put on the screen ", hex16, ".", cr, 0x00  ;;
    ;; kprintf(kterm_normal, msg) var16                                       ;;
    ;; ;; Next some code.                                                     ;;
    ;;                                                                        ;;
    ;; The above code will put on the screen the text: "This text will be put ;;
    ;; on the screen 0x2216.\n\0". NOTE: the \n and the \0 are here in the C  ;;
    ;; style for simplicity only.                                             ;;
    ;;                                                                        ;;
    ;;---------------------------------stack----------------------------------;;
    ;;                                                                        ;;
    ;; �������������������ͻ    This is for explanation only.                 ;;
    ;; �        ...        �     - Something before.                          ;;
    ;; �     ktermMode     �     - The terminal mode of 16 bits long.         ;;
    ;; �       n_var       �     - The number of vars of 16 bits.             ;;
    ;; �        str*       �     - The string pointer to print.               ;;
    ;; �       var...      �     - The vars pointer (That MUST BE IN ORDER).  ;;
    ;; �        ...        �     - The std call parameters.                   ;;
    ;; �������������������ͼ                                                  ;;
    ;;                                                                        ;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    _kprintf:
        pushad                          ; Save current stat of the registers
        mov ebp, esp                    ; in the stack.
        
        mov esi, [ebp + 0x0c]           ; Load the string pointer.
        
        xor cx, cx                      ; Initialize the var pointer
        
        ;======================================================================;
        ; This part check the content of the string to print it correctly.     ;
        ;======================================================================;
        .check:
            mov al, byte [esi]          ; Copy the current char of the string.
            inc esi                     ; Make esi pointing to the next char.
            
            cmp al, null                ; If string terminason found and then 
            je .ret                     ; goto .ret.
            
            cmp al, bin
            je .bin
        
            cmp al, oct
            je .oct
        
            cmp al, hex
            je .hex
        
            cmp al, dec
            je .dec
        
            cmp al, bel
            je .bel
        
            cmp al, bs
            je .bs
        
            cmp al, tab
            je .tab
        
            cmp al, lf
            je .lf
        
            cmp al, vt
            je .vt
        
            cmp al, ff
            je .ff
        
            cmp al, cr
            je .cr
        
            cmp al, esc
            je .esc
                                                                
        ;======================================================================;
        ; This part will only put any other char on the screen at the current  ;
        ; cursor position.                                                     ;
        ;======================================================================;
        .char:
            mov ebx, [kterm.scBase]     ; Load the current cursor position.

;            add ebx, ebx                ; Fastest way to multiply per two the 
                                        ; value of ebx. That's to have the word
                                        ; boundary of the displacement in the
                                        ; screen.
                                        
;            add ebx, [kterm.scBase]     ; Add the base addresse of the terminal.
            
            mov ah,  [ebp + 0x09]       ; Load the terminal mode to ah.
            
            mov word [ebx], ax          ; Print the char.
            
            inc word [kterm.scBase]          ; Inc the cursor to the next char. 
            inc word [kterm.scBase]          ; Inc the cursor to the next char.
            
            jmp .check
        
        ;======================================================================;
        ; This part is for ending the string (when al == null).                ;
        ;======================================================================;
        .ret:
            popad
            
            jmp kterm.setVgaCur
            
        ;======================================================================;
        ; TODO: Writing the bel char or it's representation on the screen. I   ;
        ; think that part is not extremly important for the moment but if      ;
        ; someone want to do it,he will be welcome.                            ;
        ;======================================================================;
        .bel:
        .bin:
        .oct:
        .hex:
        .dec:
        .lf:
        .vt:
        .ff:
        .cr:
        .esc:
            jmp .check
            
        ;======================================================================;
        ; TODO: Writing the bel char or it's representation on the screen. I   ;
        ; think that part is not extremly important for the moment but if      ;
        ; someone want to do it,he will be welcome.                            ;
        ;======================================================================;
        .bs:
            sub dword [kterm.cursor], 0x01
            
            mov ebx, [kterm.cursor]
            add ebx, ebx
            add ebx, [kterm.scBase]
            
            mov [ebx], word 0x0000
        
            jmp .check        

        ;======================================================================;
        ; This part is to print tabulation on the screen.                      ;
        ;======================================================================;
        .tab:
        	jmp .check
          
    kterm.setVgaCur:
		mov ebx, [kterm.cursor]
		xor edx, edx
				
		mov dx, 0x03D4	; Affecte la valeur 0x03D4 au registre dx.
		mov al, 0x0E	; Affecte la valeur 0x0E au registre al.
		out dx, al		; Envoie de al au port de sortie dx.
			
		mov dx, 0x03D5	; Affecte la valeur 0x03D5 au registre dx.
		mov al, bh		; Affecte le registre BH au registre al.
		out dx, al		; Envoie de al au port de sortie dx.

		mov dx, 0x03D4	; Affecte la valeur 0x03D4 au registre dx.
		mov al, 0x0F	; Affecte la valeur 0x0F au registre al.
		out dx, al		; Envoie de al au port de sortie dx.

		mov dx, 0x03D5	; Affecte la valeur 0x03D5 au registre dx.
		mov al, bl		; Affecte le registre BL au registre al.
		out dx, al		; Envoie de al au port de sortie dx.

		ret
  
    
%endif ; __F_CORE_KTERM_ASM__


