;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011 Maxime Jeanson for the Zircon Project.                    ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;----------------------------------core.asm----------------------------------;;
;;                                                                            ;;
;; This file contain the core system of the Zircon Project.                   ;;
;;                                                                            ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef __F_CORE_ASM__
%define __F_CORE_ASM__
	
	[ORG 0x100000]
	[BITS 32]

	%define MULTIBOOT_HEADER_MAGIC  0x1BADB002
	%define MULTIBOOT_HEADER_FLAGS  0x00010003
	%define CHECKSUM -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)

	%define MULTIBOOT_MAGIC 0x2BADB002

	align 4
	multibootHeader:
		dd MULTIBOOT_HEADER_MAGIC
		dd MULTIBOOT_HEADER_FLAGS
		dd CHECKSUM
		
		dd multibootHeader
		dd multibootHeader
		dd 0x00
		dd 0x00
		dd coreStart
	align 1
	
	%include "core/kterm.inc"
	%include "core/kterm.asm"
	
	coreStart:

		kprintf(kterm_normal, coreInit)
		kprintf(kterm_warning, coreInit)
		kprintf(kterm_error, coreInit)
		kprintf(kterm_fatal, coreInit)

		.hlt:
			hlt
			jmp .hlt
			
			
		coreInit: db 'Core system initialization, please wait!...', bin, oct, hex, dec, cr, 0x00		
%endif ; __F_CORE_ASM__
