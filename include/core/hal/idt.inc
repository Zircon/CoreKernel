%ifndef __F_HAL_IDT_INC__
%define __F_HAL_IDT_INC__
	
	namespace __idt
		uint32 .ptr,	0x00000800
		uint16 .size,	0x07ff
		
		lbl init
			clearmem([__idt.ptr])
			
		endlbl
	endnamespace

%endif ; __F_HAL_IDT_INC__