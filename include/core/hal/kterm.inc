%ifndef __F_CORE_KTERM_INC__
%define __F_CORE_KTERM_INC__

	%define kterm_warning	0x0e
	%define kterm_fatal		0x04
	%define kterm_error		0x06
	%define kterm_good		0x02
	%define kterm_normal	0x07

	%define null	0x00
	%define hex		0x01
	%define bel		0x07
	%define bs		0x08
	%define tab		0x09
	%define lf		0x0a
	%define vt		0x0b
	%define ff		0x0c
	%define cr		0x0d
	%define esc		0x0e

	kterm.cursor: dd 0x000b8000

	;--------------------------------------------------------------------------;

	%define kprintf(mode, ptr, vars) kprintf mode, ptr, vars

	;--------------------------------------------------------------------------;

	%macro kprintf 2-*
		push word	%1
		push dword	%2

		call kprintf

		add esp, 0x06
	%endmacro

%endif ; __F_CORE_KTERM_INC__
