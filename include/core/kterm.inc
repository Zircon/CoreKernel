%ifndef __F_CORE_KTERM_INC__
%define __F_CORE_KTERM_INC__

	%define kterm_warning	0x0e
	%define kterm_fatal		0x04
	%define kterm_error		0x06
	%define kterm_good		0x02
	%define kterm_normal	0x07

	%define null	0x00
	%define bin		0x01
	%define oct		0x02
	%define hex		0x03
	%define dec		0x04
	%define bel		0x07
	%define bs		0x08
	%define tab		0x09
	%define lf		0x0a
	%define vt		0x0b
	%define ff		0x0c
	%define cr		0x0d
	%define esc		0x0e

;	kterm.cursor: dd 0x000b8000
	uint16 kterm.cursor, 0x0000
	
	uint32 kterm.monitor.base, 0x000b8000
	uint16 kterm.monitor.xSize, 0x50
	uint16 kterm.monitor.ySize, 0x19

	;--------------------------------------------------------------------------;

	%define kprintf(mode, ptr) kprintf mode, ptr

	;--------------------------------------------------------------------------;

	%macro kprintf 2
		pushad
		push word %1
		push dword %2

		call __kprintf

		add esp, dword 0x06
		popad
	%endmacro

%endif ; __F_CORE_KTERM_INC__
