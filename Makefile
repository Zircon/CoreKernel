ASM=nasm
FORMAT=bin
FLAGS= -f $(FORMAT) -o $(OBJDIR)/$@ -P "include/nasm/nasm" -I $(INCLUDEDIR)
BUILDDIR=build
OBJDIR=$(BUILDDIR)/objects
INCLUDEDIR="include/"

SOURCES=core.o kterm.o

ARCHIVE=@gzip 
DATE=@date

BACKUP=zircon.tar

zircon.krnl: $(SOURCES)
	@cat $(OBJDIR)/core.o > $@

install: zircon.krnl
	@sh ./install

backup:
	@tar -cf zircon.tar *
	$(ARCHIVE) -9 $(BACKUP)
	

clean: zircon.krnl
	@rm ./zircon.krnl
	@rm ./$(OBJDIR)/*.o
	

include ./*.mkfile


